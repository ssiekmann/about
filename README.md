# About Sascha

If you are reading this, you clicked on either in a link on my Confluence space or I made you aware of it. Either way, I hope you will find it useful.

I live in Portland, Oregon on the Pacific Coast of the United States. That means my timezone is PDT (GMT-8) and my regular working hours are 8am - 5pm Monday through Friday.

You can reach me through Teams, Skype, iMessage, WhatsApp, Signal and of course, email.

I am protective of uninterrupted, "quality" work time. Interruptions cause loss of focus and context switches cause productivity losses. Therefore, you may not get an immediate reply on instant messaging or email. This is intentional.

https://www.nohello.com

If you need my immediate attention and think your matter is important, please call me on my cell.

I will try and reply to your email in an effective manner, meaning I will be mindful of the length of my reply. You might see this in my signature:

http://five.sentenc.es

In project work, I prefer collaboration using systems such as JIRA. Simply create an issue for me to work on. I'll get on it.